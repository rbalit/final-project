-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 19, 2016 at 04:20 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ski_meetups`
--

-- --------------------------------------------------------

--
-- Table structure for table `destinations`
--

CREATE TABLE `destinations` (
  `idDestination` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `location` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `destinations`
--

INSERT INTO `destinations` (`idDestination`, `title`, `location`, `description`, `image`) VALUES
(1, 'Verbier', 'Switzerland', 'Lorem Ipsum', ''),
(2, 'Zermatt', 'Switzerland', 'Lorem Ipsum', ''),
(3, 'Chamonix', 'France', 'Lorem Ipsum', ''),
(4, 'Tignes', 'France', 'Lorem Ipsum', ''),
(5, 'hello', 'ccc', 'ccc', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `idUser` int(11) NOT NULL,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `email` varchar(150) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(20) NOT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `nationality` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`idUser`, `firstname`, `lastname`, `email`, `username`, `password`, `gender`, `nationality`) VALUES
(1, 'admin', 'admin', 'admin@admin.com', 'admin01', 'admin', '0', NULL),
(2, 'andrea', 'gelsomino', 'andrea@erere.it', 'gelso', '1234', '0', 'Italy'),
(4, 'dssddf', 'g', 'g@g.co', 'g', 'g', 'male', 'wwww'),
(5, 'dssddf', 'g', 'g@g.co', 'g', 'g', 'male', 'wwww'),
(6, 'dssddf', 'g', 'g@g.co', 'g', 'g', 'male', 'wwww'),
(7, 'dssddf', 'g', 'g@g.co', 'g', 'g', 'male', 'wwww'),
(8, 'dssddf', 'g', 'g@g.co', 'g', 'g', 'male', 'wwww'),
(9, 'dssddf', 'g', 'g@g.co', 'g', 'g', 'male', 'wwww'),
(10, 'dssddf', 'g', 'g@g.co', 'g', 'g', 'male', 'wwww'),
(11, 'sddsd', 'dsdssdds', 'amduchesneau@gmail.com', '123', '123', 'male', 'FGFGFG'),
(12, 'Roxanne', 'Balit', 'roxanne.balit@bts.tech', 'rbalit', 'hello', 'male', 'canada'),
(13, 'dssddf', 'g', 'g@g.co', 'g', 'g', 'male', 'wwww'),
(14, 'barcelona', 'barcelona', 'barcelona@barcelona.es', 'barcelona', 'barcelona', 'male', 'spain'),
(15, 's', 's', 's@s.com', 's', 's', 'male', 's'),
(16, 'dssddf', 'g', 'g@g.co', 'g', 'g', 'male', 'wwww'),
(17, 'a', 'a', 'a@a.com', 'a', 'a', 'male', 'a'),
(18, 'null', 'null', 'null', 'null', 'null', 'null', 'null'),
(19, 'f', 'f', 'f@f.com', 'f', 'f', 'male', 'f'),
(20, 's', 's', 's@s.com', 'ss', 's', 'male', 's'),
(21, 'ss', 's', 's@s.com', 's', 's', 'male', 's'),
(22, 'ss', 's', 's@s.com', 's', 's', 'male', 's'),
(23, 'c', 'c', 'c@c.com', 'c', 'c', 'male', 'c'),
(24, 'ff', 'f', 'f@f.com', 'f', 'f', 'male', 'f'),
(25, 'Anne Marie', 'Duchesneau', 'amduchesneau@gmail.com', 'am', 'aaa', 'male', 'aaa'),
(26, 'arin', 'soleymani', 'arin@arin.com', 'arin', 'arin', 'male', 'sweden'),
(27, 'Roxanne', 'Balit', 'r@r.com', 'r', 'r', 'male', 'can'),
(28, 'null', 'null', 'null', 'null', 'null', 'null', 'null');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `destinations`
--
ALTER TABLE `destinations`
  ADD PRIMARY KEY (`idDestination`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idUser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `destinations`
--
ALTER TABLE `destinations`
  MODIFY `idDestination` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
