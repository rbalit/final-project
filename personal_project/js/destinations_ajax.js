$( document ).ready(function() {




/* manage data list */
function manageData() {
    $.ajax({
        dataType: 'json',
        url: 'http://localhost:8080/ski_meetups/destinations.jsp',
    }).done(function(data){
        

        manageRow(data);
        

    });

}


/* Read Data */

function manageRow(data) {
    var rows = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr>';
        rows = rows + '<td>'+value.idDestination+'</td>';
        rows = rows + '<td>'+value.title+'</td>';
        rows = rows + '<td data-id="'+value.loaction+'">';
        rows = rows + '<td data-id="'+value.image+'">';
        rows = rows + '<button data-toggle="modal" data-target="#edit-item" class="btn btn-primary edit-item">Edit</button> ';
        rows = rows + '<button class="btn btn-danger remove-item">Delete</button>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });

    $("tbody").html(rows);
}

