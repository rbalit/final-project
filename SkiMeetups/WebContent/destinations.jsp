<%@page import="java.io.PrintWriter"%>
<%@page language="java" import="java.sql.*"%>
<%@page import="java.util.*" %>
<%@page contentType="text/json; charset=UTF-8"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@page import="org.json.simple.parser.ParseException"%>


<%
response.addHeader("Access-Control-Allow-Origin", "*");

	
    String sql  = "SELECT * FROM destinations";
 
    try
    {
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn=null;
        conn=DriverManager.getConnection("jdbc:mysql://localhost/ski_meetups","root","root");
        ResultSet rs=null;
        Statement stm1=conn.createStatement();
 
        JSONArray list = new JSONArray();
        rs=stm1.executeQuery(sql);
        while(rs.next())
        {
            JSONObject obj=new JSONObject();
            obj.put("idDestination", rs.getString("idDestination"));
            obj.put("title", rs.getString("title"));
            obj.put("location", rs.getString("location"));
            obj.put("image", rs.getString("image"));
            //obj.put("addr", rs.getString("address"));
 
            list.add(obj);
        }
 
     
        //Salida por pantalla del navegador la lista de departamentos
        out.print(list);
        
       //Clase PrintWriter usada para poder imprimir un mensaje en el navegador.
       //PrintWriter out2 = response.getWriter();
		//out.println("hola");
		//out2.flush();
        
        
        
    }
    catch(Exception ex)
    {
        out.println("<h1>"+ex+"</g1>");
    }
%>